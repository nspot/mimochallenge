//
//  ViewManager.swift
//  MimoiOSCodingChallenge
//
//  Created by Norbert Spot on 20/04/2017.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit

@objc class ViewManager: NSObject {

    class func switchToView(_ viewToSwitchTo: UIViewController) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window.rootViewController = viewToSwitchTo
    }
    
}
