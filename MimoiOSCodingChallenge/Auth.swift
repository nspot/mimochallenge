//
//  Auth.swift
//  MimoiOSCodingChallenge
//
//  Created by Norbert Spot on 12/04/2017.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit
import Alamofire

@objc class Auth: NSObject {
    
    let domain = "https://mimo-test.auth0.com"
    let clientId = "PAn11swGbMAVXVDbSCpnITx5Utsxz1co"
    let connection = "Username-Password-Authentication"
    let scope = "openid profile email"
    
    // MARK: Login
    
    func login(_ username: String, _ password: String, completion: @escaping (_ response: String?, _ success: Bool) -> Void) {
        let parameters: Parameters = [
            "client_id": clientId,
            "username": username,
            "password": password,
            "connection": connection,
            "scope": scope
        ]
        
        Alamofire.request("\(domain)/oauth/ro", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { [unowned self] response in
            switch response.result {
            case .success:
                if let json = response.result.value as? [String:Any] {
                    if let accessToken = json["access_token"] as? String {
                        let defaults = UserDefaults.standard
                        
                        // MARK: Note
                        // should be stored in the KeyChain
                        defaults.set(accessToken, forKey: "access_token")
                        
                        self.userinfo(accessToken, completion: { response in
                            if let email = response["email"] as? String, let avatarUrl = response["picture"] as? String {
                                defaults.set(email, forKey: "email")
                                defaults.set(avatarUrl, forKey: "avatarUrl")
                                defaults.synchronize()
                                completion(nil, true)
                            }
                        })
                    }
                    
                    if let description = json["error_description"] as? String {
                        completion(description, false)
                    }
                }
            case .failure(let error):
                completion("login failed with error: \(error)", false)
            }
            
        }
    }
    
    // MARK: Signup
    
    func signup(_ username: String, _ password: String, completion: @escaping (_ response: String?) -> Void) {
        let parameters: Parameters = [
            "client_id": clientId,
            "email": username,
            "password": password,
            "connection": connection,
        ]
        
        Alamofire.request("\(domain)/dbconnections/signup", method: .post, parameters: parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let json = response.result.value as? [String:Any] {
                    if let description = json["description"] as? String {
                        completion(description)
                    } else {
                        completion("signup successful, please verify your email")
                    }
                }
            case .failure(let error):
                completion("signup failed with error: \(error)")
            }
            
        }
    }
    
    // MARK: User Profile
    
    func userinfo(_ accessToken: String, completion: @escaping (_ response: [String:Any]) -> Void) {
        if let url = URL(string: "\(domain)/userinfo") {
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = HTTPMethod.get.rawValue
            urlRequest.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            
            Alamofire.request(urlRequest).responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    if let json = response.result.value as? [String:Any] {
                        completion(json)
                    }
                case .failure:
                    break
                }
            })
        }
    }
    
    // MARK: Pop-up
    
    func showMessage(_ description: String, _ inViewController: UIViewController) {
        let alertController = UIAlertController(title: nil, message: description, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        inViewController.present(alertController, animated: true, completion: nil)
    }

}

extension String {
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}
