//
//  LoginViewController.swift
//  MimoiOSCodingChallenge
//
//  Created by Norbert Spot on 12/04/2017.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit

@objc class LoginViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tableView: UITableView!
    let auth = Auth()
    
    var usernameField: UITextField?
    var passwordField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView = UITableView(frame: CGRect(), style: .grouped)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(LoginTableViewCell.self, forCellReuseIdentifier: "loginCell")
        tableView.register(LoginButtonTableViewCell.self, forCellReuseIdentifier: "button")
        tableView.keyboardDismissMode = .interactive
        
        view.addSubview(tableView)
        
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|", options: [], metrics: nil, views: ["tableView": tableView]))
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tableView]|", options: [], metrics: nil, views: ["tableView": tableView]))
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "loginCell", for: indexPath) as! LoginTableViewCell
            cell.selectionStyle = .none
            
            switch indexPath.row {
            case 0:
                cell.textField.placeholder = "email"
                usernameField = cell.textField
            case 1:
                cell.textField.placeholder = "password"
                cell.textField.isSecureTextEntry = true
                passwordField = cell.textField
            default:
                break
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "button", for: indexPath) as! LoginButtonTableViewCell
            
            switch indexPath.row {
            case 0:
                cell.title = "Login"
            case 1:
                cell.title = "Signup"
            default:
                break
            }
            
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 1 {
            switch indexPath.row {
            case 0:
                if let username = usernameField?.text, let password = passwordField?.text {
                    if username.isValidEmail() {
                        auth.login(username, password, completion: { [weak self] (response, success) in
                            if let weakSelf = self {
                                if let response = response {
                                    weakSelf.auth.showMessage(response, weakSelf)
                                }
                                
                                if success {
                                    let settingsView = SettingsViewController()
                                    ViewManager.switchToView(settingsView)
                                }
                            }
                        })
                    } else {
                        auth.showMessage("Please enter a correct email", self)
                    }
                }
                
            case 1:
                if let username = usernameField?.text, let password = passwordField?.text {
                    if username.isValidEmail() {
                        auth.signup(username, password, completion: { [weak self] (response) in
                            if let weakSelf = self {
                                if let response = response {
                                    weakSelf.auth.showMessage(response, weakSelf)
                                }
                            }
                        })
                    } else {
                        auth.showMessage("Please enter a correct email", self)
                    }
                }
            default:
                break
            }
        }
        
    }

}
