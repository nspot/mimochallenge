//
//  LoginTableViewCell.swift
//  MimoiOSCodingChallenge
//
//  Created by Norbert Spot on 12/04/2017.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit

class LoginTableViewCell: UITableViewCell {
    
    let textField = UITextField()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.borderStyle = .none
        textField.font = UIFont.systemFont(ofSize: 17.0)
        textField.textColor = UIColor.darkGray
        textField.autocapitalizationType = .none
        
        contentView.addSubview(textField)
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[textField]-|", options: [], metrics: nil, views: ["textField": textField]))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[textField]|", options: [], metrics: nil, views: ["textField": textField]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
