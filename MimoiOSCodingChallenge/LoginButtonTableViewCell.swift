//
//  LoginButtonTableViewCell.swift
//  MimoiOSCodingChallenge
//
//  Created by Norbert Spot on 12/04/2017.
//  Copyright © 2017 Mimohello GmbH. All rights reserved.
//

import UIKit

class LoginButtonTableViewCell: UITableViewCell {
    
    var title: String? {
        didSet {
            label.text = title
        }
    }
    
    private let label = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 17.0)
        label.textColor = UIColor.darkGray
        label.textAlignment = .center
        label.backgroundColor = UIColor.clear
        
        contentView.addSubview(label)
        
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-[label]-|", options: [], metrics: nil, views: ["label": label]))
        contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[label]|", options: [], metrics: nil, views: ["label": label]))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
